from django import template

register = template.Library()


@register.filter
def addition_cut(value):
    # кастомный темплейт фильтр
    return value.replace(' ', '').lower()
